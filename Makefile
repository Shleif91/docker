USER := shleif

dev:
	@docker-compose stop && \
		 sudo docker-compose up -d --build && \
		 sudo chown -R $(USER):$(USER) ./app && \
		 sudo chmod 777 -R app/var/cache && \
		 sudo chmod 777 -R app/var/logs && \
		 sudo chmod 777 -R app/var/sessions

down:
	@docker-compose down

stop:
	@docker-compose stop

start:
	@docker-compose start

chmod:
	@sudo chown -R $(USER):$(USER) ./app
